# Clementine Player hotkeys for Windows

Written by [AutoHotkey](https://autohotkey.com) scripting language. 

## Download

You can download `clementine_ahk.exe` from this repository. Also, you can use below permalink(s):

[Version 0.1](https://gitlab.com/shosseinib/clementinehotkeys/raw/master/clementine_ahk.exe)

## Usage
##### Note 0:

This script works with default clementine installation directory in 64-bit Windows versions, `C:\Program Files (x86)\Clementine`. If you customized installing destination, you must modify `clementine.ahk` and run/compile via *AutoHotkey*.

##### Note 1: 
*If clementine is not oppened, any hotkey can run it.*

##### Note 2: 
*If you want to use these hotkeys at each windows session, you must add the binary file to the Windows start-up directory/settings, according to your Windows version.*

#### Hotkeys:

`Win + Return` : Play/Pause

`Win + \` : Stop

`Win + ]` : Next track

`Win + [` : Previous track

`Win + /` : Show OSD (on-screen track discription)

## License

[GNU General Public License](https://www.gnu.org/licenses/gpl.html)

*See also: AutoHotkey [license](https://www.autohotkey.com/docs/license.htm).*

*Icon source: [http://www.iconarchive.com/show/plex-icons-by-cornmanthe3rd/Media-clementine-icon.html](http://www.iconarchive.com/show/plex-icons-by-cornmanthe3rd/Media-clementine-icon.html)*